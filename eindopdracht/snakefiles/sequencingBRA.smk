# -*- python -*-
from os.path import join
import glob
import os

configfile: "/homes/sarends/jaar_3/Thema_11/dataprocessing/dataprocessing/eindopdracht/snakefiles/sequencingBRA.yaml"
workdir: config["wdir"]

# List containing the complete file name of all files
basenameBRA = [os.path.basename(x).split(".")[0] for x in glob.glob(config["samplesBRA"] + "*")]
# List containing 22 stripped file names
BowtieInput = [os.path.basename(x).rstrip("1_001.fastq.gz") for x in glob.glob(config["samplesBRA"] + "*") if x.endswith("1_001.fastq.gz")]


rule fastqcBRA:
    """"
    Rule fastqcBRA is responsible for the quality control of 
    the reads.
    
    The inputs are the original
    .fastq/.fastq.gz files and the outputs of the rule create_index.
    """
    input:
        bt2 = expand(config["datadir"] + "bowtie_index/macaca.{ind}.bt2", ind=range(1,5)),
        rev = expand(config["datadir"] + "bowtie_index/macaca.rev.{ind}.bt2", ind=range(1,3)),
        sampleBRA = config["samplesBRA"] + "{sampleBRA}.fastq.gz"
    benchmark:
        join(config["outdir"], "benchmarks/fastqc/fastqcBRA/{sampleBRA}_benchmark.txt")
    log:
        join(config["outdir"], "log/fastqc/fastqcBRA/{sampleBRA}.log")
    output:
        o1 = temp(config["datadir"] + "fastqc/fastqcBRA/{sampleBRA}_fastqc.html"),
        o2 = temp(config["datadir"] + "fastqc/fastqcBRA/{sampleBRA}_fastqc.zip")
    message: "Executing fastqc to determine the quality of the files"
    threads: 60
    shell:
        "(fastqc {input.sampleBRA} --threads 60 -o {config[datadir]}fastqc/fastqcBRA/) 2> {log}"

rule intermediate_step:
    """
    Rule intermediate_step makes sure that the transition from
    the rule fastqcBRA and bowtieBRA geo smoothly.
    
    The inputs are the outputs of the rule fatsqcBRA.
    
    The output is a .txt file indicating that this step is done.
    """
    input:
        expand(config["datadir"] + "fastqc/fastqcBRA/{sampleBRA}_fastqc.html", sampleBRA = basenameBRA),
        expand(config["datadir"] + "fastqc/fastqcBRA/{sampleBRA}_fastqc.zip", sampleBRA = basenameBRA)
    output:
        temp(config["datadir"] + "intermediate_step.txt")
    shell:
        "touch {config[datadir]}intermediate_step.txt"

rule bowtieBRA:
    """"
    Rule bowtieBRA aligns the reads to an index of a reference genome.
    
    The inputs are the output of the rule intermediate_step and the original
    .fastq/.fastq.gz files.
    
    The outputs are new files that are aligned to a reference genomen
    in the format of .sam files.
    """
    input:
        config["datadir"] + "intermediate_step.txt",
        sample1 = config["samplesBRA"] + "{sampleBow}1_001.fastq.gz",
        sample2 = config["samplesBRA"] + "{sampleBow}2_001.fastq.gz"
    output:
        temp(config["datadir"] + "bowtie_align/bowtieBRA/{sampleBow}1_001.sam")
    log:
        join(config["outdir"], "log/bowtie/bowtieBRA/{sampleBow}1_001.log")
    benchmark:
        join(config["outdir"], "benchmarks/bowtie/bowtieBRA/{sampleBow}1_001_benchmark.txt")
    message: "Executing bowtie aligner on the Brazil cohort"
    threads: 60
    shell:
         "(bowtie2 -x {config[bowtie_index]} -1 {input.sample1} -2 {input.sample2} -S {output} -p {threads}) 2> {log}"

rule picardSortBRA:
    """"
    Rule picardSortBRA sorts the reads.
    
    The inputs are .sam generated in the rule
    bowtieBRA.
    
    The outputs are the new sorted files which are also transformed to 
    .bam files.
    """
    input:
        samplesBRA = config["datadir"] + "bowtie_align/bowtieBRA/{sampleBow}1_001.sam"
    output:
        temp(config["datadir"] + "picard/picardSort/picardBRA/{sampleBow}1_001.bam")
    log:
        join(config["outdir"], "log/picard/picardSort/picsort_BRA/{sampleBow}1_001.log")
    benchmark:
        join(config["outdir"], "benchmarks/picard/picardSort/picsort_BRA/{sampleBow}1_001_benchmark.txt")
    message: "Executing bowtie aligner on the Brazil cohort"
    shell:
        "(PicardCommandLine SortSam INPUT={input.samplesBRA} OUTPUT={output} SORT_ORDER=queryname) 2> {log}"

rule picardReadGroupsBRA:
    """"
    Rule picardReadGroupsBRA assigns the reads to a new read-group
    
    The inputs are each .bam file generated from the rule
    picardSortBRA.
    
    The outputs are new.bam file where the read groups have been added.
    """
    input:
        samplesBRA = config["datadir"] + "picard/picardSort/picardBRA/{sampleBow}1_001.bam"
    output:
          temp(config["datadir"] + "picard/picardRG/picardBRA/{sampleBow}1_001.bam")
    message: "Executing PicardComandLine AddOrReplaceReadGroups on the Brazil cohort"
    log:
        join(config["outdir"], "log/picard/picardRG/picRG_BRA/{sampleBow}1_001.log")
    benchmark:
        join(config["outdir"], "benchmarks/picard/picardRG/picRG_BRA/{sampleBow}1_001_benchmark.txt")
    shell:
        "(PicardCommandLine AddOrReplaceReadGroups INPUT={input.samplesBRA} OUTPUT={output} RGID=4 RGLB=lib1 RGPL=illumina RGPU=unit1 RGSM=20) 2> {log}"

rule picardFixMateInformationBRA:
    """"
    Rule picardFixMateInformationBRA verifies mate-pair information between mates.
    
    The inputs are each .bam file generated in the rule
    picardReadGroupsBRA.
    
    The outputs are new .bam files where the mate-pair information
    has been verified.
    """
    input:
         samplesBRA = config["datadir"] + "picard/picardRG/picardBRA/{sampleBow}1_001.bam"
    output:
          temp(config["datadir"] + "picard/picardFixMate/picardBRA/{sampleBow}1_001.bam")
    message: "Executing PicardCommandLine FixMateInformation on the Brazil cohort"
    log:
        join(config["outdir"], "log/picard/picardFixMate/picFMI_BRA/{sampleBow}1_001.log")
    benchmark:
        join(config["outdir"], "benchmarks/picard/picardFixMate/picFMI_BRA/{sampleBow}1_001_benchmark.txt")
    shell:
         "(PicardCommandLine FixMateInformation INPUT={input.samplesBRA} OUTPUT={output} ADD_MATE_CIGAR=true) 2> {log}"

rule picardMarkDuplicatesBRA:
    """"
    Rule picardMarkDuplicatesBRA locates and tags duplicate reads.
    
    The inputs are each .bam file generated in the 
    rule picardFixMateInformationBRA.
    
    The outputs are new .bam files where the duplicates reads have been
    tagged.
    """
    input:
         samplesBRA = config["datadir"] + "picard/picardFixMate/picardBRA/{sampleBow}1_001.bam"
    output:
          out = temp(config["datadir"] + "picard/picardMarkDuplicates/picardBRA/{sampleBow}1_001.bam"),
          metrics = temp(config["datadir"] + "picard/picardMarkDuplicates/metrics/picardBRA/{sampleBow}1_001_metrics.txt")
    message: "Executing PicardCommandLine MarkDuplicates on the Brazil cohort"
    log:
        join(config["outdir"], "log/picard/picardMarkDup/picMDUP_BRA/{sampleBow}1_001.log")
    benchmark:
        join(config["outdir"], "benchmarks/picard/picardMarkDup/picMDUP_BRA/{sampleBow}1_001_benchmark.txt")
    shell:
         "(PicardCommandLine MarkDuplicates INPUT={input.samplesBRA} OUTPUT={output.out} METRICS_FILE={output.metrics}) 2> {log}"

rule samtoolsSortBRA:
    """"
    Rule samtoolsSortBRA sorts the files produced
    in the picardMarkDuplicatesBRA rule.
    
    The inputs are each .bam and .txt file generated 
    from the rule picardMatkDuplicatesBRA.
    
    The outputs are each .bam file sorted.
    """
    input:
        samples = config["datadir"] + "picard/picardMarkDuplicates/picardBRA/{sampleBow}1_001.bam",
        metrics = config["datadir"] + "picard/picardMarkDuplicates/metrics/picardBRA/{sampleBow}1_001_metrics.txt"
    output:
        temp(config["datadir"] + "samtools/sort/samSortBRA/{sampleBow}1_001.bam")
    message: "Executing samtools sort on the Brazil cohort"
    log:
        join(config["outdir"], "log/samtools/sort/samSortBRA/{sampleBow}1_001.log")
    benchmark:
        join(config["outdir"], "benchmarks/samtools/sort/samSortBRA/{sampleBow}1_001_benchmark.txt")
    shell:
         "(samtools sort -n {input.samples} -o {output}) 2> {log}"

