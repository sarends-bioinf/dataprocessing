# -*- python -*-
from os.path import join
import glob
import os

configfile: "/homes/sarends/jaar_3/Thema_11/dataprocessing/dataprocessing/eindopdracht/snakefiles/sequencingNL_BRA.yaml"

include: "sequencingBRA.smk"
include: "sequencingNl.smk"

rule all:
    input:
        config["outdir"] + "results_images/pca.jpg",
        config["outdir"] + "results_images/heatmap.jpg",
        config["outdir"] + "results_images/MDS.jpg"
    message:
        "Executing of the pipeline has been completed."

def featrueCount_inputsNL(wildcards):
    """"
    Returns a list of Dutch file names

    :return files - List
    """
    files = expand(config["datadir"] + "samtools/sort/samSortNL/{sampleNL}.bam", sampleNL=basenameNL)
    return files


def featrueCount_inputsBRA(wildcards):
    """"
    Returns the list of Brazilian file names

    :return files - List
    """
    files = expand(config["datadir"] + "samtools/sort/samSortBRA/{sampleBow}1_001.bam", sampleBow=BowtieInput)
    return files


rule featureCounts:
    """"
    Rule featureCounts executes the bash commando
    featureCounts. This commando generates a gene
    expression count file.
    
    The input consist of two list containing
    the file names generated in the 
    samtoolSort rule for both Dutch and
    Brazilian cohort.
    
    Output is a gene expression count file.
    
    """
    input:
        filesNL = featrueCount_inputsNL,
        filesBRA = featrueCount_inputsBRA
    output:
        config["datadir"] + "featureCounts/featureCount_NL_BRA_data.txt"
    message: "Executing featureCounts on the Dutch and Brazilian cohort"
    threads: 60
    log:
        join(config["outdir"], "log/featureCounts/featureCounts.log")
    benchmark:
        join(config["outdir"], "benchmarks/featureCounts/featureCounts_benchmark.txt")
    shell:
        "(featureCounts -a {config[gtf]} -o {output} {input.filesNL} {input.filesBRA} -T {threads}) 2> {log}"

rule plot_images:
    """
    Rule plot_images calls an Rscript that generates the final
    result. 
    
    The input is the output generated in the featureCounts rule.
    
    Outputs are three .jpg files.
    """
    input:
        config["datadir"] + "featureCounts/featureCount_NL_BRA_data.txt"
    output:
        pca = config["outdir"] + "results_images/pca.jpg",
        heatmap = config["outdir"] + "results_images/heatmap.jpg",
        MDS = config["outdir"] + "results_images/MDS.jpg"
    shell:
         "Rscript {config[rscript]} {input} {output.pca} {output.heatmap} {output.MDS}"