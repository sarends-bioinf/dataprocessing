# -*- python -*-
from os.path import join
import glob
import os

configfile: "/homes/sarends/jaar_3/Thema_11/dataprocessing/dataprocessing/eindopdracht/snakefiles/configNL.yaml"
workdir: config["wdir"]

# List containing the complete file names of th Dutch cohort
basenameNL = [os.path.basename(x).split(".")[0] for x in glob.glob(config["samplesNL"] + "*")]

def returnFullNameNL(baseName):
    """"
    Receives the basename of a file. Loops through
    a list of fill names and tries to match the basename
    to these file names. If one file name matches the basename
    it is returned.

    :param basename - String
    :return file - String
    """
    file = ""
    for x in glob.glob(config["samplesNL"] + "*"):
        if x.find(baseName) != -1:
            file += x
    return file

rule create_index:
    """"
    Rule create index creates an index of reference genome.
    
    The inputs is a reference genome.
    
    The outputs are the newly created index.
    """
    input:
        config["genome"]
    log:
        out = config["outdir"] + "log/bwa_index/macaca.index.log"
    threads: 80
    benchmark:
        join(config["outdir"], "benchmarks/bwa_index/benchmark_index.txt")
    output:
        bt2 = temp(expand(config["datadir"] + "bowtie_index/macaca.{ind}.bt2", ind=range(1,5))),
        rev = temp(expand(config["datadir"] + "bowtie_index/macaca.rev.{ind}.bt2", ind=range(1,3)))
    message: "Executing bowtie2-build to generate a index that is suitable for the use of bowtie2, that will be used later in the pipeline"
    shell:
        "(bowtie2-build -f {input} {config[bowtie_index]} --threads {threads}) 2> {log.out} "

rule fastqc:
    """"
    Rule fastqc is responsible for the quality control of 
    the reads.
    
    The inputs are the original
    .fastq/.fastq.gz files and the outputs of the rule create_index.
    """
    input:
        bt2 = expand(config["datadir"] + "bowtie_index/macaca.{ind}.bt2", ind=range(1,5)),
        rev = expand(config["datadir"] + "bowtie_index/macaca.rev.{ind}.bt2", ind=range(1,3)),
        sampleNL = lambda wildcards: returnFullNameNL('{sampleNL}'.format(sampleNL=wildcards.sampleNL))
    benchmark:
        join(config["outdir"], "benchmarks/fastqc/fastqcNL/{sampleNL}_benchmark.txt")
    log:
        join(config["outdir"], "log/fastqc/fastqcNL/{sampleNL}.log")
    output:
        o1 = temp(config["datadir"] + "fastqc/fastqcNL/{sampleNL}_fastqc.zip"),
        o2 = temp(config["datadir"] + "fastqc/fastqcNL/{sampleNL}_fastqc.html")
    message: "Executing fastqc to determine the quality of the files"
    threads: 60
    shell:
        "(fastqc {input.sampleNL} -o {config[datadir]}fastqc/fastqcNL/ -t {threads}) 2> {log}"

rule bowtie:
    """"
    Rule bowtie aligns the reads to an index of a reference genome.
    
    The inputs are the outputs of the rule fastqc and the original
    .fastq/.fastq.gz files.
    
    The outputs are new files that are aligned to a reference genomen
    in the format of .sam files.
    """
    input:
        config["datadir"] + "fastqc/fastqcNL/{sampleNL}_fastqc.zip",
        config["datadir"] + "fastqc/fastqcNL/{sampleNL}_fastqc.html",
        sampleNL = lambda wildcards: returnFullNameNL('{sampleNL}'.format(sampleNL=wildcards.sampleNL))
    output:
        temp(config["datadir"] + "bowtie_align/bowtieNL/{sampleNL}.sam")
    log:
        join(config["outdir"], "log/bowtie/bowtieNL/{sampleNL}.log")
    benchmark:
        join(config["outdir"], "benchmarks/bowtie/bowtieNL/{sampleNL}_benchmark.txt")
    message: "Executing bowtie aligner on the Dutch cohort"
    threads: 60
    shell:
         "(bowtie2 -x {config[bowtie_index]} -U {input.sampleNL} -S {output} -p {threads}) 2> {log}"

rule picardSort:
    """"
    Rule picardSort sorts the reads.
    
    The inputs are .sam generated in the rule
    bowtie.
    
    The outputs are the new sorted files which are also transformed to 
    .bam files.
    """
    input:
        samplesNL = config["datadir"] + "bowtie_align/bowtieNL/{sampleNL}.sam"
    output:
        temp(config["datadir"] + "picard/picardSort/picardNL/{sampleNL}.bam")
    log:
        join(config["outdir"], "log/picard/picardSort/picsort_NL/{sampleNL}.log")
    benchmark:
        join(config["outdir"], "benchmarks/picard/picardSort/picsort_NL/{sampleNL}_benchmark.txt")
    message: "Executing bowtie aligner on the Dutch cohort"
    shell:
        "(PicardCommandLine SortSam INPUT={input.samplesNL} OUTPUT={output} SORT_ORDER=queryname) 2> {log}"

rule picardReadGroups:
    """"
    Rule picardReadGroups assigns the reads to a new read-group
    
    The inputs are each .bam file generated from the rule
    picardSort.
    
    The outputs are new.bam file where the read groups have been added.
    """
    input:
        samplesNL = config["datadir"] + "picard/picardSort/picardNL/{sampleNL}.bam"
    output:
          temp(config["datadir"] + "picard/picardRG/picardNL/{sampleNL}.bam")
    message: "Executing PicardComandLine AddOrReplaceReadGroups on the Dutch cohort"
    log:
        join(config["outdir"], "log/picard/picardRG/picRG_NL/{sampleNL}.log")
    benchmark:
        join(config["outdir"], "benchmarks/picard/picardRG/picRG_NL/{sampleNL}_benchmark.txt")
    shell:
        "(PicardCommandLine AddOrReplaceReadGroups INPUT={input.samplesNL} OUTPUT={output} RGID=4 RGLB=lib1 RGPL=illumina RGPU=unit1 RGSM=20) 2> {log}"


rule picardFixMateInformation:
    """"
    Rule picardFixMateInformation verifies mate-pair information between mates.
    
    The inputs are each .bam file generated in the rule
    picardReadGroups.
    
    The outputs are new .bam files where the mate-pair information
    has been verified.
    """
    input:
         samplesNL = config["datadir"] + "picard/picardRG/picardNL/{sampleNL}.bam"
    output:
          temp(config["datadir"] + "picard/picardFixMate/picardNL/{sampleNL}.bam")
    message: "Executing PicardCommandLine FixMateInformation on the Dutch cohort"
    log:
        join(config["outdir"], "log/picard/picardFixMate/picFMI_NL/{sampleNL}.log")
    benchmark:
        join(config["outdir"], "benchmarks/picard/picardFixMate/picFMI_NL/{sampleNL}_benchmark.txt")
    shell:
         "(PicardCommandLine FixMateInformation INPUT={input.samplesNL} OUTPUT={output} ADD_MATE_CIGAR=true) 2> {log}"

rule picardMarkDuplicates:
    """"
    Rule picardMarkDuplicates locates and tags duplicate reads.
    
    The inputs are each .bam file generated in the 
    rule picardFixMateInformation.
    
    The outputs are new .bam files where the duplicates reads have been
    tagged.
    """
    input:
         samplesNL = config["datadir"] + "picard/picardFixMate/picardNL/{sampleNL}.bam"
    output:
          out = temp(config["datadir"] + "picard/picardMarkDuplicates/picardNL/{sampleNL}.bam"),
          metrics = temp(config["datadir"] + "picard/picardMarkDuplicates/metrics/picardNL/{sampleNL}_metrics.txt")
    message: "Executing PicardCommandLine MarkDuplicates on the Dutch cohort"
    log:
        join(config["outdir"], "log/picard/picardMarkDup/picMDUP_NL/{sampleNL}.log")
    benchmark:
        join(config["outdir"], "benchmarks/picard/picardMarkDup/picMDUP_NL/{sampleNL}_benchmark.txt")
    shell:
         "(PicardCommandLine MarkDuplicates INPUT={input.samplesNL} OUTPUT={output.out} METRICS_FILE={output.metrics}) 2> {log}"

rule samtoolsSort:
    """"
    Rule samtoolsSort sorts the files produced
    in the picardMarkDuplicates rule.
    
    The inputs are each .bam and .txt file generated 
    from the rule picardMatkDuplicates.
    
    The outputs are each .bam file sorted.
    """
    input:
        samples = config["datadir"] + "picard/picardMarkDuplicates/picardNL/{sampleNL}.bam",
        metrics = config["datadir"] + "picard/picardMarkDuplicates/metrics/picardNL/{sampleNL}_metrics.txt"
    output:
        temp(config["datadir"] + "samtools/sort/samSortNL/{sampleNL}.bam")
    message: "Executing samtools sort on the Dutch cohort"
    log:
        join(config["outdir"], "log/samtools/sort/samSortNL/{sampleNL}.log")
    benchmark:
        join(config["outdir"], "benchmarks/samtools/sort/samSortNL/{sampleNL}_benchmark.txt")
    shell:
         "(samtools sort -n {input.samples} -o {output}) 2> {log}"