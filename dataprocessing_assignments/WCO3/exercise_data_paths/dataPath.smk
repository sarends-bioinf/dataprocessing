# -*- python -*-
from os.path import join

SAMPLES = ["A", "B", "C", "D"]
workdir: "/students/2019-2020/Thema11/Dataprocessing/WCO2/data/"
outdir = "/homes/sarends/jaar_3/Thema_11/dataprocessing/dataprocessingwcos/WCO3/exercise_data_paths/"


rule all:
    input:
        join(outdir,"output.html")
        #expand('sorted_reads/{sample}.bam.bai', sample = SAMPLES),


rule bwa_map:
    input:
         genome ="genome.fa",
         sample ="samples/{sample}.fastq"
         #sample = "data/samples/{sample}.fastq"
         #"data/samples/A.fastq"
    output:
          join(outdir,"mapped_reads/{sample}.bam")
          #"mapped_reads/A.bam"
    benchmark:
        join(outdir,"benchmarks/{sample}.bwa.benchmark.txt")
    message:
        "executing bwa mem on the following {input} to generate the following {output}"
    shell:
         "bwa mem {input.genome} {input.sample} | samtools view -Sb - > {output}"
         #"bowtie2 -X {input.genome} -U {input.sample} -S {output}"

rule samtools_sort:
    input:
         join(outdir,"mapped_reads/{sample}.bam")
    output:
         join(outdir,"sorted_reads/{sample}.bam")
    message:
         "executing samtools sort on the following {input} to generate the following {output}."
    shell:
         "samtools sort -T sorted_reads/{wildcards.sample}"
         " -O bam {input} > {output}"

rule sametools_index:
    input:
         join(outdir, "sorted_reads/{sample}.bam")
    output:
          join(outdir,"sorted_reads/{sample}.bam.bai")
    message:
        "executing samtools index on the following {input} to generate the following {output}."
    shell:
         "samtools index {input}"

rule bcftools_call:
    input:
        fa = "genome.fa",
        bam = expand(outdir + "sorted_reads/{sample}.bam", sample = SAMPLES),
        bai = expand(outdir + "sorted_reads/{sample}.bam.bai", sample = SAMPLES)
    output:
        join(outdir,"calls/all.vcf")
    message:
        "executing samtools and bcftools on the following {input} to generate the following {output}."
    shell:
        "samtools mpileup -g -f {input.fa} {input.bam} | "
        "bcftools call -mv - > {output}"

rule report:
    input:
         join(outdir,"calls/all.vcf")
    output:
          join(outdir,"output.html")
    run:
        from snakemake.utils import report
        with open(input[0]) as f:
            n_calls = sum(1 for line in f if not line.startswith("#"))

        report("""
        An example workflow
        ===================================

        Reads were mapped to the Yeas reference genome 
        and variants were called jointly with
        SAMtools/BCFtools.

        This resulted in {n_calls} variants (see Table T1_).
        """, output[0], metadata="Author: Stijn Arends", T1=input[0])