# RNA-sequencing for microglia cells in Rhesus macaque #

![image](eindopdracht/images/sequencingNL_BRA.png)

## Description

This snakemake pipeline is used for RNA-sequencing of microglia cells in Rhesus macaques. 
To obtain a big population, two different cohorts were obtained. One cohort was sequenced in Groningen 
and the other cohort was sequenced in Brazil. Both two cohorts have their own workflow that will come together at one 
point in the pipeline. A count file will be generated and loaded into R to be further processed and transformed to produce
the final images. 

## Data

The data is split up in two cohorts. The Dutch and Brazilian cohort. The Dutch cohort
consist of 27 samples that contain single-end reads. The Brazilian cohort consist of 44 samples
that contain paired-end reads.

The reference genome used in bowtie2-build to build an index can be downloaded [here](ftp://ftp.ensembl.org/pub/release-94/fasta/macaca_mulatta/dna/Macaca_mulatta.Mmul_8.0.1.dna.toplevel.fa.gz).
The gene annotation file used for featureCounts can be downloaded [here](https://www.unmc.edu/rhesusgenechip/RhesusGenomeUpload/MacaM_Rhesus_Genome_Annotation_v7.6.8.gtf.zip).

## Workflow

The steps of the procces are described as:

*  building of an index
*  determening the quality of the reads
*  alignmed of the reads on a reference genome
*  sorting the reads
*  assigning the reads to a new read-group
*  verify mate-pair information between mates
*  locating and tagging of duplicate reads
*  sorting the reads
*  generation of a gene expression count file
*  generation of images


*for a complete overview check out the dag files in the image folder*

## Results

An example of one of the result images:

![image](eindopdracht/results_images/pca.jpg)

## Installation

The first two things that are required to run this pipeline are snakemake and a virtual environment.  
To make sure that both are installed/available follow these steps:  

```bash
	# create virtual environment
	virtualenv -p /usr/bin/python3 venv
	# activate venv
	source venv/bin/activate

	#install snakemake
	pip3 install snakemake

```

Furthermore a selection of tools are required for RNA-sequencing:

1. Commandline:

	* bowtie2 - v2.3.4.3
	* fastqc - v0.11.8
	* picard SortSam - v2.18.25
	* picard FixMateInformation - v2.18.25
	* picard MarkDuplicates - v2.1825
	* samtools sort - v1.9
	* featureCounts (Subreads package) - v1.6.3

2. R:

	* DESeq2 - v1.22.2
	* pheatmap - v1.0.12
	* ggplot2 - v3.2.1


In addition both the programming languages python and R are required. Python version 3.7.3 and R version 3.5.2 were used.

The first tool to be installed is bowtie2. Bowtie2 is used for creating a index and aligning the reads. In order to 
install bowtie2 use the following command:


```bash

sudo apt-get install -y bowtie2

```

The second tool that is required is fastqc. Fastqc is a tool to allows the user to do some quality control checks on raw 
sequence data coming from high throughput sequencing pipelines. It provides a modular set of analyses which you can use to give a 
quick impression of whether your data has any problems of which you should be aware before doing any further analysis.
With the following command you can install fastqc:

```bash

sudo apt-get install -y fastqc

```

In addition fastqc is a java application and in order to run, it needs your system to have a suitable
Java Runtime Environment (JRE) installed. That can be installed using the fllowing command:

```bash

sudo apt install default-jre

```

The different picard tools used in this pipeline are all apart of [Picard Tools](https://broadinstitute.github.io/picard/).
Picard tools requires jave to be installed, the latest jave version can be installed from [here](https://www.oracle.com/java/technologies/javase-downloads.html).
After java is installed the latest version of picard tools needs to be [downloaded](https://github.com/broadinstitute/picard/releases/tag/2.22.2).
Open the downloaded package and place the folder containing the jar file in a convenient directory on your hard drive (or server).
To test that you can run Picard tools, run the following command in your terminal application, providing either the full path to the picard.jar file:

```bash

java -jar /path/to/picard.jar -h 

```

To run any of the tools available in picard run this command:

```bash

java jvm-args -jar picard.jar PicardToolName OPTION1=value1 OPTION2=value2...

```

To get samtools running first install the latest version of [samtools](https://sourceforge.net/projects/samtools/files/samtools/). Then unzip the file:

```bash

tar xvjf samtools-1.1.tar.bz2 

```

Go into the newly created directory and compile the code by typing make:

```bash

cd samtools-1.1
make

```

Modify your .bashrc file so that when you type "samtools" it calls the program:

```bash

export PATH=$PATH:/directory/samtools-0.1.19 

```

The final tool that needs to be installed is featureCounts. First Download the Subread source package 
from [SourceForge website](https://sourceforge.net/projects/subread/files/) to your local directory. Type the following command to uncompress it:

```bash

tar zxvf subread-1.x.x.tar.gz

```

Enter the src subdirectory under the home directory of the package and then issue the following command to build it on a Linux/unix computer:

```bash

make -f Makefile.Linux

```

To make sure that the required R package are installed and loaded in use the following commands:

```R
# DESeq2
install.packages("DESeq2")
library("DESeq2")

# pheatmap
install.packages("pheatmap")
library("pheatmap")

# ggplot2
install.packages("ggplot2")
library("ggplot2")

```

## File paths

The pipeline consists of three snakefiles, those three snakefile also come with three different config.yaml files. 
These config files contain paths to the input files, Rscripts, output directories, genome location, and working directories. 
In order to run this pipeline make sure to adjust these paths to suit your own working envirnoment. 

For example:

```YAML

	# Bowtie index
	bowtie_index: /<filePath>/<index basename>
	genome: /<filePath>/Macaca_mulatta.Mmul_8.0.1.dna.toplevel.fa.gz.gz
	gtf : '/<filePath>/MacaM_Rhesus_Genome_Annotation_v7.6.8.gtf

```

In this example the filePath needs to be adjusted so that it links to the location of the index, genome and gtf file.


## Running the pipeline

In order to run the pipeline the virtual environment needs to be activated first. Go to the path where the 
virtual environment is created and activate it with this command:

```bash

source venv/bin/activate

```

Once the venv is activated run the snakefile with the following command:

```bash

snakemake --snakefile eindopdracht/snakefiles/sequencingNL_BRA.smk

```
In total there are three different snakefiles, one for the Dutch cohort, one for the Brazilian cohort and one 
that includes both these snakefiles to finnaly call the rscript that creates the final result. In the example above the snakefile that 
includes both these other snakefiles is runned. There is no need to run the other two script because that is automatically done by running the sequencingNL_BRA.smk
snakefile.

There is no need to define --cores because snakemake grabs all the available cores at the moment the script is runned.
The threads that will be used when executing the different rules are defined in each rule. When the amount
of threads specified exceeds the available threads it will automatically scale the threads down to the available amount.

## License

Check out the [license](LICENSE.md) for this project.

## Author

Stijn Arends  
Bio-informatics student Hanzehogeschool Groningen  
s.arends@st.hanze.nl  
06-04-2020

